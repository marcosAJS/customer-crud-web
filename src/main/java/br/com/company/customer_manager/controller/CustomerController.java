package br.com.company.customer_manager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.company.customer_manager.entity.Customer;
import br.com.company.customer_manager.service.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Customer>> findAll() {
		List<Customer> customers = customerService.findAll();
		return ResponseEntity.ok().body(customers);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Customer> findById(@PathVariable Long id) {
		return ResponseEntity.ok().body(customerService.FindById(id));
	}

	@PostMapping(produces = "application/json")
	public ResponseEntity<Customer> add(@RequestBody Customer customer) {
		return ResponseEntity.ok().body(customerService.create(customer));
	}

	@RequestMapping(path = "/", method = RequestMethod.PUT)
	public ResponseEntity<Customer> update(Customer customer) {
		Customer result = customerService.update(customer);
		if (result != null)
			return ResponseEntity.ok().body(result);
		else
			return ResponseEntity.badRequest().body(customer);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> remove(@PathVariable Long id) {
		Boolean result = customerService.delete(id);
		if (result)
			return ResponseEntity.ok().body(result);
		else
			return ResponseEntity.badRequest().body(result);
//	@RequestMapping(value = PATH)
//	public String error() {
//		return "Error handling";
//	}
//
//	@Override
//	public String getErrorPath() {
//		return PATH;
//	}}
	}
}
