package br.com.company.customer_manager.controller;

import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.company.customer_manager.crypto.Crypto;
import br.com.company.customer_manager.entity.User;
import br.com.company.customer_manager.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(path = "/auth", method = RequestMethod.POST)
	public ResponseEntity<?> auth(@RequestParam String username, @RequestParam String password) throws NoSuchAlgorithmException {
		System.out.println(username + " - "+Crypto.toSha256Hex(password));
		User result = userService.auth(username, Crypto.toSha256Hex(password));
		System.out.println("[DEBUG] AUTH RESULT " + result);
		if (result != null) {
			return ResponseEntity.ok().body("Usuario autenticado");
		}
		return ResponseEntity.badRequest().body("Usuario/Senha incorretos");
	}
}
