package br.com.company.customer_manager.constants;

public enum PhoneTypeEnum {

	COMERCIAL(1, "comercial"), RESIDENTIAL(2, "residiencial"), CELLPHONE(3, "celular");

	private final Integer id;
	private final String desc;

	private PhoneTypeEnum(Integer id, String desc) {
		this.id = id;
		this.desc = desc;
	}

	public Integer getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}
}
