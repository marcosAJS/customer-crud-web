package br.com.company.customer_manager.constants;

public enum UserRoleEnum {

	ADMIN(1, "administrador"), READ_ONLY(2, "somente leitura");

	private final Integer id;
	private final String desc;

	private UserRoleEnum(Integer id, String desc) {
		this.id = id;
		this.desc = desc;
	}

	public Integer getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}
}
