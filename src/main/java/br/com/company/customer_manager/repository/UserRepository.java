package br.com.company.customer_manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.company.customer_manager.entity.User;


/**
 * Specifies methods used to authenticate user related information
 * which is stored in the database.
 * @author Marcos Alex J. Santos
 *  */
public interface UserRepository extends JpaRepository<User, Long> {

	 /**
     * Finds a user by using the username and password.
     * @param username
     * @param password
     * @return  A user is an exact match with the given username and password.
     *          If no persons is found, this method returns null.
     */
	@Query("SELECT u FROM User u WHERE u.username = :username AND u.password = :password")
	public User auth(@Param("username") String username, @Param("password") String password);
}

