package br.com.company.customer_manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.company.customer_manager.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
