package br.com.company.customer_manager.service;

import java.util.List;

import br.com.company.customer_manager.entity.Customer;

public interface ICustomerService {
	public List<Customer> findAll();

	public Customer FindById(Long id);

	public Customer create(Customer customer);

	public Customer update(Customer customer);

	public Boolean delete(Long id);
}
