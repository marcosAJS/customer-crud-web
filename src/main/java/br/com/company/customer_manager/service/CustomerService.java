package br.com.company.customer_manager.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.company.customer_manager.entity.Customer;
import br.com.company.customer_manager.repository.CustomerRepository;


@Service
public class CustomerService implements ICustomerService {

	@Autowired
	CustomerRepository customerRepository;

	public List<Customer> findAll() {
		return  (List<Customer>) customerRepository.findAll();
	}

	public Customer FindById(Long id) {
		return customerRepository.findById(id).orElseGet(null);
	}

	public Customer create(Customer customer) {
		return customerRepository.save(customer);
	}

	public Customer update(Customer customer) {
		Optional<Customer> found = customerRepository.findById(customer.getId());
		if (found.isPresent()) {
			return customerRepository.save(customer);
		}
		return null;
	}

	public Boolean delete(Long id) {
		Optional<Customer> found = customerRepository.findById(Long.valueOf(id));
		if (found.isPresent()) {
			try {
				customerRepository.delete(found.get());
				return Boolean.TRUE;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return Boolean.FALSE;
	}
	
	
}
