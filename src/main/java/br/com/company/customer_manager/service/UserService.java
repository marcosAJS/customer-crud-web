package br.com.company.customer_manager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.company.customer_manager.entity.User;
import br.com.company.customer_manager.repository.UserRepository;


@Service
public class UserService implements IUserService {

	@Autowired
	UserRepository userRepository;
	
	public User auth (String username, String password) {
		User user = userRepository.auth(username, password);
		return user != null ? user : null;
	}
}
