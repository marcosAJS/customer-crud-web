package br.com.company.customer_manager.service;

import br.com.company.customer_manager.entity.User;

public interface IUserService {
	public User auth(String username, String password);
}
