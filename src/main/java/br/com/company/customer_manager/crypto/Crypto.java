package br.com.company.customer_manager.crypto;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.bouncycastle.util.encoders.Hex;

public class Crypto {

	public static String toSha256Hex(String value) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(value.getBytes(StandardCharsets.UTF_8));
		String sha256hex = new String(Hex.encode(hash));
		System.out.println(sha256hex);
		return sha256hex;
	}
	
}
